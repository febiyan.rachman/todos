/**
 * A function to find item in array with a promise.
 * Non-blocking.
 */
export default (array, item, key) => {
  return Promise((resolve, reject) => {
    const index = array.find(record => record[key] == item[key]);
    if (index !== -1) {
      resolve(index);
    } else {
      reject(index);
    }
  });
};
