import _ from "lodash";

export default type => ({
  SUCCESS: `${type}_SUCCESS`,
  FAILURE: `${type}_FAILURE`,
  PENDING: `${type}_PENDING`,
  isPendingKey: _.camelCase(`${type}_IS_PENDING`),
  errorMessageKey: _.camelCase(`${type}_ERROR_MESSAGE`)
});
