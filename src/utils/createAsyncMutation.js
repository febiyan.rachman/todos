import Vue from "vue";

export default type => ({
  [type.PENDING](state) {
    state[type.isPendingKey] = true;
  },
  [type.SUCCESS](state, payload) {
    state[type.isPendingKey] = false;
    Vue.set(state, type.dataKey, payload);
  },
  [type.FAILURE](state, payload) {
    state[type.isPendingKey] = false;
    Vue.set(state, type.errorKey, payload);
  }
});
