import axios from "axios";

export default (
  store,
  { axiosConfig, successHandler, errorHandler, mutationType }
) => {
  store.commit(mutationType.PENDING);
  return axios(axiosConfig)
    .then(response => {
      if (successHandler) {
        store.commit(mutationType.SUCCESS, successHandler(response));
      } else {
        store.commit(mutationType.SUCCESS);
      }
    })
    .catch(error => {
      if (errorHandler) {
        store.commit(mutationType.FAILURE, errorHandler(error));
      } else {
        store.commit(mutationType.FAILURE);
      }
    });
};
