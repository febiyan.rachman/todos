import Vue from "vue";
import Vuex from "vuex";
// This can be replaced by dynamically loading stores in each modules' index.vue
import identity from "@/modules/identity/store";
import todos from "@/modules/todos/store";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    identity,
    todos
  }
});
