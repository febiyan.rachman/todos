import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Login from "@/views/Login.vue";
import store from "@/store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    meta: { transitionName: "fade" }
  },
  {
    path: "/login",
    name: "login",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Login,
    // () => import(/* webpackChunkName: "login" */ "../views/Login.vue")
    meta: { transitionName: "fade" }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

// Route guard
router.beforeEach((to, from, next) => {
  if (!store.getters["identity/isAuthenticated"]) {
    if (to.path.includes("/login")) {
      next();
    } else {
      next("/login");
    }
  } else {
    if (to.path.includes("/login")) {
      next("/");
    } else {
      next();
    }
  }
});

export default router;
