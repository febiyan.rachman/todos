import Vue from "vue";
import * as mutationTypes from "./mutationTypes";
import initialState from "./state";

export default {
  [mutationTypes.LOGIN.PENDING]: function(state) {
    Vue.set(state, mutationTypes.LOGIN.isPendingKey, true);
  },
  [mutationTypes.LOGIN.SUCCESS]: function(state, payload) {
    Vue.set(state, mutationTypes.LOGIN.isPendingKey, false);
    Object.keys(payload).forEach(key => {
      Vue.set(state, key, payload[key]);
    });
  },
  [mutationTypes.LOGIN.FAILURE]: function(state, payload) {
    Vue.set(state, mutationTypes.LOGIN.isPendingKey, false);
    Vue.set(state, mutationTypes.LOGIN.errorMessageKey, payload);
  },
  [mutationTypes.LOGOUT.SUCCESS]: function(state) {
    Object.keys(initialState).forEach(key => {
      Vue.set(state, key, null);
    });
  }
};
