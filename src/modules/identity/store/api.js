/**
 * Mock authentication function
 * @param {*} credentials;
 */
export async function authenticate(credentials) {
  const validIdentity = {
    username: "febiyan",
    displayName: "Febiyan",
    password: "febiyan123",
    authToken: "234567890",
    authTokenExpiredAt: null
  };

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (
        validIdentity.username === credentials.username &&
        validIdentity.password === credentials.password
      ) {
        delete credentials.password;
        resolve(validIdentity);
      } else {
        reject(new Error("Wrong username or password."));
      }
    }, 1000);
  });
}
