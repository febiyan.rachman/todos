import createAsyncMutationType from "@/utils/createAsyncMutationType.js";

export const LOGIN = createAsyncMutationType("LOGIN");
export const LOGOUT = createAsyncMutationType("LOGOUT");
