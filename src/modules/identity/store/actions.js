import * as identityApi from "./api";
import * as mutationTypes from "./mutationTypes";
import getAxiosErrorMessage from "@/utils/getAxiosErrorMessage";

export default {
  async login(store, credentials) {
    store.commit(mutationTypes.LOGIN.PENDING);
    return identityApi
      .authenticate(credentials)
      .then(response => {
        // TODO: Proper handling of HTTP response
        localStorage.setItem("todo-app-user", JSON.stringify(response));
        store.commit(mutationTypes.LOGIN.SUCCESS, response);
      })
      .catch(error => {
        // TODO: Proper handling of Axios errors
        store.commit(mutationTypes.LOGIN.FAILURE, getAxiosErrorMessage(error));
      });
  },
  logout(store) {
    localStorage.removeItem("todo-app-user");
    store.commit(mutationTypes.LOGOUT.SUCCESS);
  }
};
