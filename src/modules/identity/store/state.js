import * as mutationTypes from "./mutationTypes";

export default {
  username: null,
  displayName: null,
  authToken: null,
  authTokenExpiredAt: null,
  [mutationTypes.LOGIN.isPendingKey]: null,
  [mutationTypes.LOGIN.errorMessageKey]: null,
  ...JSON.parse(localStorage.getItem("todo-app-user"))
};
