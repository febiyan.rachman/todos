import createAsyncMutationType from "@/utils/createAsyncMutationType.js";

export const ITEMS_LOAD = createAsyncMutationType("ITEMS_LOAD");
export const ITEM_ADD = createAsyncMutationType("ITEM_ADD");
export const ITEM_REMOVE = createAsyncMutationType("ITEM_REMOVE");
export const ITEM_UPDATE = createAsyncMutationType("ITEM_UPDATE");
