// import doAsync from "@/utils/doAsync";
import * as mutationTypes from "./mutationTypes";
import asyncArrayFindByKey from "@/utils/asyncArrayFindByKey";

// Mock the data
let mockLoadTodoItems = [
  {
    id: 1,
    text: "Don't forget to make a todo list",
    image: "https://via.placeholder.com/500x300.png?text=Image%201",
    completed: false
  },
  {
    id: 2,
    text: "Dont for",
    image: "https://via.placeholder.com/500x300.png?text=Image%202",
    completed: false
  }
];

export default {
  /**
   * Load items
   * @param {*} store vue store context
   */
  loadItems(store) {
    store.commit(mutationTypes.ITEMS_LOAD.SUCCESS, mockLoadTodoItems);
  },
  /**
   * Add a todo item to list
   * @param {*} store vue store context
   * @param {*} item item to be added
   */
  addItem(store, item) {
    const newId = store.state.items[store.state.items.length - 1].id + 1;
    store.commit(mutationTypes.ITEM_ADD.SUCCESS, {
      ...item,
      id: newId
    });
  },
  /**
   * Remove a todo item from the list
   * @param {*} store vue store context
   * @param {*} item todo item to be removed
   */
  removeItem(store, item) {
    asyncArrayFindByKey(store.state.items, item, "id").then(index => {
      store.commit(mutationTypes.ITEM_REMOVE.SUCCESS, {
        item,
        index
      });
    });
  },
  /**
   * Update a todo item from the list
   * @param {*} store vue store context
   * @param {*} updatedItem updated item with the same id
   */
  updateItem(store, updatedItem) {
    asyncArrayFindByKey(store.state.items, updatedItem, "id").then(index => {
      store.commit(mutationTypes.ITEM_UPDATE.SUCCESS, {
        updatedItem,
        index
      });
    });
  }
};
