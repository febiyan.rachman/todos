/*
 * Expected todo item structure:
 * {
 *  id,
 *  text,
 *  imageUrl,
 *  isCompleted,
 *  isSynced
 * }
 */
export default {
  todos: []
};
