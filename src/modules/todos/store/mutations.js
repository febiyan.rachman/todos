import * as mutationTypes from "./mutationTypes";
import Vue from "vue";

// import createAsyncMutation from "@/utils/createAsyncMutation";

export default {
  [mutationTypes.ITEM_REMOVE.SUCCESS](state, { item, index }) {
    if (state.items[index].id == item.id) {
      state.items.splice(index, 1);
    }
  },
  [mutationTypes.ITEM_UPDATE.SUCCESS](state, { item, index }) {
    if (state.items[index].id == item.id) {
      Vue.set(state.items, index, item);
    }
  },
  [mutationTypes.ITEM_ADD.SUCCESS](state, item) {
    state.items.push(item);
  },
  [mutationTypes.ITEM_REMOVE.SUCCESS](state, item) {
    state.items.push(item);
  }
  /* // Mutations that talk to an API
  ...createAsyncMutation(mutationTypes.ITEM_ADD),
  ...createAsyncMutation(mutationTypes.ITEM_REMOVE) */
};
